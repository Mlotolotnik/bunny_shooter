import pygame
import time

pygame.init()

class ScreenGen:
    font = pygame.font.Font(None,100)
    
    def play_game_intro(self, screen, SCREEN_WIDTH, SCREEN_HEIGHT):
        text = "Get ready!"
        image_text = self.font.render(text, True, (255,255,255))
        screen.blit(image_text, (SCREEN_WIDTH // 2 - image_text.get_width() // 2, SCREEN_HEIGHT // 2 - image_text.get_height() // 2))
        pygame.display.flip()
        time.sleep(1)
        pass
    def play_game_over(self, screen, SCREEN_WIDTH, SCREEN_HEIGHT):
        text = "GAME OVER"
        image_text = self.font.render(text, True, (255,255,255))
        screen.blit(image_text, (SCREEN_WIDTH // 2 - image_text.get_width() // 2, SCREEN_HEIGHT // 2 - image_text.get_height() // 2))
        pygame.display.flip()
        pass
    def play_level_intro(self, level_number, screen, SCREEN_WIDTH, SCREEN_HEIGHT):
        text = f"Level {level_number}"
        image_text = self.font.render(text, True, (255,255,255))
        screen.blit(image_text, (SCREEN_WIDTH // 2 - image_text.get_width() // 2, SCREEN_HEIGHT // 2 - image_text.get_height() // 2))
        pygame.display.flip()
        time.sleep(1)
        pass
    def play_level_outro(self, screen, SCREEN_WIDTH, SCREEN_HEIGHT):
        text = "Well done!"
        image_text = self.font.render(text, True, (255,255,255))
        screen.blit(image_text, (SCREEN_WIDTH // 2 - image_text.get_width() // 2, SCREEN_HEIGHT // 2 - image_text.get_height() // 2))
        pygame.display.flip()
        time.sleep(1)
        pass
