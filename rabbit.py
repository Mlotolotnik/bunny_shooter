import random
import pygame

class Rabbit:
    def __init__(self):
        self.rabbit_image = pygame.image.load("./resources/rabbit.png")
        self.rabbit_hit_image = pygame.image.load("./resources/rabbit_hit.png")
        self.shot_miss_sound = pygame.mixer.Sound("./resources/shot_miss.mp3")
        self.shot_hit_sound = pygame.mixer.Sound("./resources/shot_hit.mp3")
        self.res_x = 800
        self.res_y = 600
        self.rabbit_position = (0,0)

        self.rabbit_visible = False
        self.rabbit_alive = False

    def randomize_rabbit_position(self):
        self.rabbit_position = (random.randint(0, self.res_x - 100), random.randint(0, self.res_y - 100))

    def rabbit_ressurrect(self):
        self.rabbit_alive = True
    def rabbit_hit(self):
        self.shot_hit_sound.play()
        self.rabbit_alive = False
    def rabbit_miss(self):
        self.shot_miss_sound.play()
    def rabbit_show(self):
        self.rabbit_visible = True
    def rabbit_hide(self):
        self.rabbit_visible = False

    def is_rabbit_hit(self, mouse_pos):
        if self.rabbit_visible == False:
            return False
        x, y = mouse_pos
        xr, yr = self.rabbit_position
        if abs(xr + 50 - x) <= 25 and abs(yr + 50 - y) <= 50:
            return True
        else:
            return False
    
