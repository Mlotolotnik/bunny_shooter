import pygame
import random
import time
from rabbit import Rabbit
from screen_generator import ScreenGen
from pygame.locals import *

pygame.init()
pygame.mixer.init()
pygame.mouse.set_cursor(pygame.cursors.broken_x)
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
background_color = (70,150,70)

heart_image = pygame.image.load("./resources/heart.png")

white_rabbit = Rabbit()
GENERAL_ALIVE_RABBIT_SHOWTIME = 2
GENERAL_DEAD_RABBIT_SHOWTIME = 1
GENERAL_PAUSE_SHOWTIME = 1
current_level = 1
rabbits_left = 10
player_lives = 3
current_gamestate = "game_intro"
screen_gen = ScreenGen()
pause_start_time = 0
rabbit_alive_start_time = 0
rabbit_dead_start_time = 0

running = True
while running:
    screen.fill(background_color) #wypełnij ekran kolorem
    if player_lives > 2:
        screen.blit(heart_image, (580,0))
    if player_lives > 1:
        screen.blit(heart_image, (650,0))
    if player_lives > 0:
        screen.blit(heart_image, (720,0))
    
    if current_gamestate == "game_intro":
        screen.fill(background_color)
        screen_gen.play_game_intro(screen, SCREEN_WIDTH, SCREEN_HEIGHT)
        current_gamestate = "level_intro"
    if current_gamestate == "level_intro":
        screen.fill(background_color)
        screen_gen.play_level_intro(current_level, screen, SCREEN_WIDTH, SCREEN_HEIGHT)
        current_gamestate = "pause"
    if current_gamestate == "level_outro":
        screen.fill(background_color)
        screen_gen.play_level_outro(screen, SCREEN_WIDTH, SCREEN_HEIGHT)
        current_level = current_level + 1
        current_gamestate = "level_intro"
    if current_gamestate == "pause":
        if rabbits_left == 0:
            rabbits_left = 10
            current_gamestate = "level_outro"
        elif pause_start_time == 0:
            pause_start_time = time.time()
            white_rabbit.rabbit_hide()
        elif time.time() > GENERAL_PAUSE_SHOWTIME / current_level + pause_start_time:
            pause_start_time = 0
            current_gamestate = "show_alive_rabbit"
    if current_gamestate == "show_alive_rabbit":
        if rabbit_alive_start_time == 0:
            rabbit_alive_start_time = time.time()
            white_rabbit.rabbit_ressurrect()
            white_rabbit.randomize_rabbit_position()
            white_rabbit.rabbit_show()
        elif time.time() > GENERAL_ALIVE_RABBIT_SHOWTIME / current_level + rabbit_alive_start_time:
            rabbit_alive_start_time = 0
            player_lives = player_lives - 1
            white_rabbit.rabbit_hide()
            if player_lives < 0:
                current_gamestate = "game_over"
            else:
                current_gamestate = "pause"
    if current_gamestate == "show_dead_rabbit":
        if rabbit_dead_start_time == 0:
            rabbit_alive_start_time = 0
            rabbit_dead_start_time = time.time()
        if time.time() > GENERAL_DEAD_RABBIT_SHOWTIME / current_level + rabbit_dead_start_time:
            rabbit_dead_start_time = 0
            current_gamestate = "pause"
    if current_gamestate == "game_over":
        screen_gen.play_game_over(screen, SCREEN_WIDTH, SCREEN_HEIGHT)
            
    #aktualizacja ekranu w zależności od tego, czy królik żyje i czy jest widoczny
    if white_rabbit.rabbit_visible:
        if white_rabbit.rabbit_alive:
            screen.blit(white_rabbit.rabbit_image, white_rabbit.rabbit_position)
        else:
            screen.blit(white_rabbit.rabbit_hit_image, white_rabbit.rabbit_position)
    pygame.display.flip() #odśwież zawartość ekranu

    
    for event in pygame.event.get():
        if event.type == QUIT:
            running = False
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            if white_rabbit.is_rabbit_hit(pygame.mouse.get_pos()):
                rabbit_alive_start_time = 0
                #królik nie żyje więc czas życia jest resetowany
                #jeżeli tego nie zrobię to gdy królik znowu będzie żył
                #to zostanie przekroczony licznik czasu na strzelanie
                #i jedno życie gracza zostanie odjęte
                rabbits_left = rabbits_left - 1
                white_rabbit.rabbit_hit()
                current_gamestate = "show_dead_rabbit"
            else:
                white_rabbit.rabbit_miss()
                
        #pomocnicza spacja testowa, w grze jej nie będzie    
##        if event.type == KEYDOWN and event.key == K_SPACE:
##            if white_rabbit.rabbit_visible == False:
##                white_rabbit.randomize_rabbit_position()
##                white_rabbit.rabbit_ressurrect()
##                white_rabbit.rabbit_show()
##            elif white_rabbit.rabbit_visible == True:
##                white_rabbit.rabbit_hide()
                
pygame.quit()
